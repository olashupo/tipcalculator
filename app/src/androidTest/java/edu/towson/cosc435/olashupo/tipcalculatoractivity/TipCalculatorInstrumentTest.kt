package edu.towson.cosc435.olashupo.tipcalculatoractivity

import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import junit.framework.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.util.regex.Pattern.matches

class TipCalculatorInstrumentTest {
    @Test
    fun calculateTenPercent(){
        onView(withId(R.id.input_value))
            .perform(typeText("100")).perform()
        onView(withId(R.id.perct_btn))
            .perform(click())
        onView(withId(R.id.calculate_btn))
            .perform(click())
        onView(withId(R.id.result_tv))

    }
}