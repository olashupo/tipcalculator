package edu.towson.cosc435.olashupo.tipcalculatoractivity

import junit.framework.Assert.assertEquals
import org.junit.Test

class TipCalculatorTests {
    @Test
    fun calculateTenPercent(){
        val result = convertTip(20.0, ConvertType.tenPer)

        assertEquals(22.0, result,0.00001)
    }
}