package edu.towson.cosc435.olashupo.tipcalculatoractivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculate_btn.setOnClickListener { handleClick() }
    }
    private fun handleClick(){
        try{
            //Toast.makeText(this,"Clicked", Toast.LENGTH_SHORT).show()

            val enteredText = input_value.editableText.toString()
            //Toast.makeText(this,"Entered: $enteredText", Toast.LENGTH_SHORT).show()

            val convertType = when(radioGroup.checkedRadioButtonId){
                R.id.perct_btn ->ConvertType.tenPer
                R.id.perct2_btn ->ConvertType.twenPer
                R.id.perct3_btn ->ConvertType.thirtyPer
                else-> throw Exception()
            }

            val enteredValue = enteredText.toDouble()

            val result = convertTip(enteredValue,convertType)
             val total = enteredValue + result

            result_tv.text = "Your calculated tip is $"+result.toString()+" and your total is $"+ total.toString()
        }catch (e: Exception){
            result_tv.text = resources.getString(R.string.error_string)
        }


    }
}
