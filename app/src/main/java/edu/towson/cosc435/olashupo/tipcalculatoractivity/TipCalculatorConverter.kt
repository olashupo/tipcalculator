package edu.towson.cosc435.olashupo.tipcalculatoractivity

enum class ConvertType{tenPer,twenPer,thirtyPer}

fun convertTip(input: Double,convertType: ConvertType ):Double{

    return  when(convertType){
          ConvertType.tenPer -> ((10/100.0)*input)
          ConvertType.twenPer -> ((20/100.0)*input)
          ConvertType.thirtyPer -> ((30/100.0)*input)
      }
}